# Smart crops - `src/db/`

This part of the application depends on:

- Nothing

## Requirements

- [`docker`](https://www.docker.com/)
- [`docker-compose`](https://docs.docker.com/compose/)
- [`git`](https://git-scm.com/)

## Launch

### Docker

```sh
# Clone the repository
git clone https://gitlab.com/smart-crops/smart-crops.git

# Move to the cloned directory
cd smart-crops

# Copy the environment variables file
cp .env.dist .env

# Edit the environment variables file
vim .env

# Start the database
docker-compose up --build db
```
