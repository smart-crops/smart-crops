# Smart crops - `src/frontend/`

This part of the application depends on:

- [`src/api/`](https://gitlab.com/smart-crops/smart-crops/tree/master/src/api)

## Requirements

- [`docker`](https://www.docker.com/)
- [`docker-compose`](https://docs.docker.com/compose/)
- [`Node.js`](https://nodejs.org/en/)
- [`npm`](https://www.npmjs.com/)
- 
## Launch

### Locally

```sh
# Clone the repository
git clone https://gitlab.com/smart-crops/smart-crops.git

# Move to the cloned directory
cd smart-crops/src/api/

# Copy the environment variables file
cp .env.dist .env

# Edit the environment variables file
vim .env

# Install the dependencies
npm install

# Execute the linter
npm run lint

# Start the UI
npm run serve
```

### Docker

```sh
# Clone the repository
git clone https://gitlab.com/smart-crops/smart-crops.git

# Move to the cloned directory
cd smart-crops

# Copy the environment variables file
cp .env.dist .env

# Edit the environment variables file
vim .env

# Start the frontend
docker-compose up --build frontend
```

## Other

This app is based on the following template made by Creative Tim (https://www.creative-tim.com/) under a MIT licence. More can be found here: [Vue Argon Dashboard](https://demos.creative-tim.com/vue-argon-dashboard).
