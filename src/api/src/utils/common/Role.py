from enum import Enum


class Role(Enum):
    ADMIN = "admin"
    MODERATOR = "moderator"
    READ_ONY = "read-only"
