import logging

from src.Database import Database
from src.utils.common.Role import Role

logger = logging.getLogger('UserService')


class UserService():

    @staticmethod
    async def addUser(
        username: str,
        password: str,
        role: Role,
    ):
        logger.info("user added.")

    @staticmethod
    async def deleteUser():
        logger.info("user deleted")

    @staticmethod
    async def getUser():
        logger.info("get user")

    @staticmethod
    async def getUsers():
        logger.info("get users")
