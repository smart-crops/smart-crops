import connexion
import six

from src.models.authorized import Authorized  # noqa: E501
from src.models.sensor_read import SensorRead  # noqa: E501
from src.models.sensor_write import SensorWrite  # noqa: E501
from src import util


def add_sensor(sensor_write):  # noqa: E501
    """add_sensor

    Create a new sensor. # noqa: E501

    :param sensor_write: The details of the sensor.
    :type sensor_write: dict | bytes

    :rtype: Authorized
    """
    if connexion.request.is_json:
        sensor_write = SensorWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def delete_sensor(sensor_id):  # noqa: E501
    """delete_sensor

    Delete the specified sensor. # noqa: E501

    :param sensor_id: The sensor ID.
    :type sensor_id: str

    :rtype: None
    """
    return 'do some magic!'


def get_sensor(sensor_id):  # noqa: E501
    """get_sensor

    Get the specified sensor. # noqa: E501

    :param sensor_id: The sensor ID.
    :type sensor_id: str

    :rtype: SensorRead
    """
    return 'do some magic!'


def get_sensors():  # noqa: E501
    """get_sensors

    Get all the sensors. # noqa: E501


    :rtype: List[SensorRead]
    """
    return 'do some magic!'


def update_sensor(sensor_id, sensor_write):  # noqa: E501
    """update_sensor

    Update the specified sensor. # noqa: E501

    :param sensor_id: The sensor ID.
    :type sensor_id: str
    :param sensor_write: The details of the sensor.
    :type sensor_write: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        sensor_write = SensorWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
