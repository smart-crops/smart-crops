import connexion
import six

from src.models.authorized import Authorized  # noqa: E501
from src.models.unit_read import UnitRead  # noqa: E501
from src.models.unit_write import UnitWrite  # noqa: E501
from src import util


def add_unit(unit_write):  # noqa: E501
    """add_unit

    Create a new unit. # noqa: E501

    :param unit_write: The details of the unit.
    :type unit_write: dict | bytes

    :rtype: Authorized
    """
    if connexion.request.is_json:
        unit_write = UnitWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def delete_unit(unit_id):  # noqa: E501
    """delete_unit

    Delete the specified unit. # noqa: E501

    :param unit_id: The unit ID.
    :type unit_id: str

    :rtype: None
    """
    return 'do some magic!'


def get_unit(unit_id):  # noqa: E501
    """get_unit

    Get the specified unit. # noqa: E501

    :param unit_id: The unit ID.
    :type unit_id: str

    :rtype: UnitRead
    """
    return 'do some magic!'


def get_units():  # noqa: E501
    """get_units

    Get all the units. # noqa: E501


    :rtype: List[UnitRead]
    """
    return 'do some magic!'


def update_unit(unit_id, unit_write):  # noqa: E501
    """update_unit

    Update the specified unit. # noqa: E501

    :param unit_id: The unit ID.
    :type unit_id: str
    :param unit_write: The details of the unit.
    :type unit_write: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        unit_write = UnitWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
