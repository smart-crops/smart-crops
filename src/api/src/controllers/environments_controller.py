import connexion
import six

from src.models.authorized import Authorized  # noqa: E501
from src.models.environment_read import EnvironmentRead  # noqa: E501
from src.models.environment_write import EnvironmentWrite  # noqa: E501
from src import util


def add_environment(environment_write):  # noqa: E501
    """add_environment

    Create a new environment. # noqa: E501

    :param environment_write: The details of the environment.
    :type environment_write: dict | bytes

    :rtype: Authorized
    """
    if connexion.request.is_json:
        environment_write = EnvironmentWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def delete_environment(environment_id):  # noqa: E501
    """delete_environment

    Delete the specified environment. # noqa: E501

    :param environment_id: The environment ID.
    :type environment_id: str

    :rtype: None
    """
    return 'do some magic!'


def get_environment(environment_id):  # noqa: E501
    """get_environment

    Get the specified environment. # noqa: E501

    :param environment_id: The environment ID.
    :type environment_id: str

    :rtype: EnvironmentRead
    """
    return 'do some magic!'


def get_environments():  # noqa: E501
    """get_environments

    Get all the environments. # noqa: E501


    :rtype: List[EnvironmentRead]
    """
    return 'do some magic!'


def update_environment(environment_id, environment_write):  # noqa: E501
    """update_environment

    Update the specified environment. # noqa: E501

    :param environment_id: The environment ID.
    :type environment_id: str
    :param environment_write: The details of the environment.
    :type environment_write: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        environment_write = EnvironmentWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
