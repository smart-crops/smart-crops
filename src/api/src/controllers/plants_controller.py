import connexion
import six

from src.models.authorized import Authorized  # noqa: E501
from src.models.plant_read import PlantRead  # noqa: E501
from src.models.plant_write import PlantWrite  # noqa: E501
from src import util


def add_plant(plant_write):  # noqa: E501
    """add_plant

    Create a new plant. # noqa: E501

    :param plant_write: The details of the plant.
    :type plant_write: dict | bytes

    :rtype: Authorized
    """
    if connexion.request.is_json:
        plant_write = PlantWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def delete_plant(plant_id):  # noqa: E501
    """delete_plant

    Delete the specified plant. # noqa: E501

    :param plant_id: The plant ID.
    :type plant_id: str

    :rtype: None
    """
    return 'do some magic!'


def get_plant(plant_id):  # noqa: E501
    """get_plant

    Get the specified plant. # noqa: E501

    :param plant_id: The plant ID.
    :type plant_id: str

    :rtype: PlantRead
    """
    return 'do some magic!'


def get_plants():  # noqa: E501
    """get_plants

    Get all the plants. # noqa: E501


    :rtype: List[PlantRead]
    """
    return 'do some magic!'


def update_plant(plant_id, plant_write):  # noqa: E501
    """update_plant

    Update the specified plant. # noqa: E501

    :param plant_id: The plant ID.
    :type plant_id: str
    :param plant_write: The details of the plant.
    :type plant_write: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        plant_write = PlantWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
