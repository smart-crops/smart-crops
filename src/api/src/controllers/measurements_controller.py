import connexion
import six

from src.models.authorized import Authorized  # noqa: E501
from src.models.measurements_pagination import MeasurementsPagination  # noqa: E501
from src.models.measurements_write import MeasurementsWrite  # noqa: E501
from src import util


def add_measurements(measurements_write):  # noqa: E501
    """add_measurements

    Add new measurements. # noqa: E501

    :param measurements_write: The details of the measurement.
    :type measurements_write: dict | bytes

    :rtype: Authorized
    """
    if connexion.request.is_json:
        measurements_write = MeasurementsWrite.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def delete_measurements(unit_id, _from, to):  # noqa: E501
    """delete_measurements

    Delete the specified measurements. # noqa: E501

    :param unit_id: The unit ID.
    :type unit_id: str
    :param _from: The starting timestamp to get the data.
    :type _from: str
    :param to: The ending timestamp to get the data.
    :type to: str

    :rtype: None
    """
    _from = util.deserialize_datetime(_from)
    to = util.deserialize_datetime(to)
    return 'do some magic!'


def get_measurements(unit_id, _from, to, start, limit):  # noqa: E501
    """get_measurements

    Get the specified measurements. # noqa: E501

    :param unit_id: The unit ID.
    :type unit_id: str
    :param _from: The starting timestamp to get the data.
    :type _from: str
    :param to: The ending timestamp to get the data.
    :type to: str
    :param start: The starting item of all data.
    :type start: int
    :param limit: The limit of the number of items to get of all data.
    :type limit: int

    :rtype: MeasurementsPagination
    """
    _from = util.deserialize_datetime(_from)
    to = util.deserialize_datetime(to)
    return 'do some magic!'
