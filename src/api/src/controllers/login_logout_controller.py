import connexion
import six

from src.models.authorized import Authorized  # noqa: E501
from src.models.credentials import Credentials  # noqa: E501
from src import util


def login(credentials):  # noqa: E501
    """login

    Login to the admin side of the application. # noqa: E501

    :param credentials: The credentials of the user.
    :type credentials: dict | bytes

    :rtype: Authorized
    """
    if connexion.request.is_json:
        credentials = Credentials.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def logout():  # noqa: E501
    """logout

    Logout from the admin side of the application. # noqa: E501


    :rtype: None
    """
    return 'do some magic!'
