from flask_pymongo import PyMongo


class Database():
    __instance = None

    def __init__(
        self,
        flask_app,
        db_username: str,
        db_password: str,
        db_hostname: str,
        db_port: int,
        db_name: str,
    ):
        if Database.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            mongo_uri = "mongodb://{}:{}@{}:{}/{}".format(
                db_username,
                db_password,
                db_hostname,
                db_port,
                db_name,
            )

            mongo = PyMongo(app=flask_app, uri=mongo_uri)

            Database.__instance = mongo.db

    @staticmethod
    def getInstance():
        """ Static access method. """
        if Database.__instance is None:
            raise Exception("This class must be instanced once!")
        else:
            return Database.__instance
