# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from src.models.authorized import Authorized  # noqa: E501
from src.models.sensor_read import SensorRead  # noqa: E501
from src.models.sensor_write import SensorWrite  # noqa: E501
from src.test import BaseTestCase


class TestSensorsController(BaseTestCase):
    """SensorsController integration test stubs"""

    def test_add_sensor(self):
        """Test case for add_sensor

        
        """
        sensor_write = SensorWrite()
        response = self.client.open(
            '/sensors',
            method='POST',
            data=json.dumps(sensor_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_sensor(self):
        """Test case for delete_sensor

        
        """
        response = self.client.open(
            '/sensors/{sensorId}'.format(sensor_id='sensor_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_sensor(self):
        """Test case for get_sensor

        
        """
        response = self.client.open(
            '/sensors/{sensorId}'.format(sensor_id='sensor_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_sensors(self):
        """Test case for get_sensors

        
        """
        response = self.client.open(
            '/sensors',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_sensor(self):
        """Test case for update_sensor

        
        """
        sensor_write = SensorWrite()
        response = self.client.open(
            '/sensors/{sensorId}'.format(sensor_id='sensor_id_example'),
            method='PATCH',
            data=json.dumps(sensor_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
