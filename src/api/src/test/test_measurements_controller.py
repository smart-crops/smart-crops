# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from src.models.authorized import Authorized  # noqa: E501
from src.models.measurements_pagination import MeasurementsPagination  # noqa: E501
from src.models.measurements_write import MeasurementsWrite  # noqa: E501
from src.test import BaseTestCase


class TestMeasurementsController(BaseTestCase):
    """MeasurementsController integration test stubs"""

    def test_add_measurements(self):
        """Test case for add_measurements

        
        """
        measurements_write = MeasurementsWrite()
        response = self.client.open(
            '/measurements',
            method='POST',
            data=json.dumps(measurements_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_measurements(self):
        """Test case for delete_measurements

        
        """
        query_string = [('_from', '2013-10-20T19:20:30+01:00'),
                        ('to', '2013-10-20T19:20:30+01:00')]
        response = self.client.open(
            '/measurements/{unitId}'.format(unit_id='unit_id_example'),
            method='DELETE',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_measurements(self):
        """Test case for get_measurements

        
        """
        query_string = [('_from', '2013-10-20T19:20:30+01:00'),
                        ('to', '2013-10-20T19:20:30+01:00'),
                        ('start', 56),
                        ('limit', 56)]
        response = self.client.open(
            '/measurements/{unitId}'.format(unit_id='unit_id_example'),
            method='GET',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
