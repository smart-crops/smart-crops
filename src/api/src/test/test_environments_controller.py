# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from src.models.authorized import Authorized  # noqa: E501
from src.models.environment_read import EnvironmentRead  # noqa: E501
from src.models.environment_write import EnvironmentWrite  # noqa: E501
from src.test import BaseTestCase


class TestEnvironmentsController(BaseTestCase):
    """EnvironmentsController integration test stubs"""

    def test_add_environment(self):
        """Test case for add_environment

        
        """
        environment_write = EnvironmentWrite()
        response = self.client.open(
            '/environments',
            method='POST',
            data=json.dumps(environment_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_environment(self):
        """Test case for delete_environment

        
        """
        response = self.client.open(
            '/environments/{environmentId}'.format(environment_id='environment_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_environment(self):
        """Test case for get_environment

        
        """
        response = self.client.open(
            '/environments/{environmentId}'.format(environment_id='environment_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_environments(self):
        """Test case for get_environments

        
        """
        response = self.client.open(
            '/environments',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_environment(self):
        """Test case for update_environment

        
        """
        environment_write = EnvironmentWrite()
        response = self.client.open(
            '/environments/{environmentId}'.format(environment_id='environment_id_example'),
            method='PATCH',
            data=json.dumps(environment_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
