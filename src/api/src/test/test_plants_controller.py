# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from src.models.authorized import Authorized  # noqa: E501
from src.models.plant_read import PlantRead  # noqa: E501
from src.models.plant_write import PlantWrite  # noqa: E501
from src.test import BaseTestCase


class TestPlantsController(BaseTestCase):
    """PlantsController integration test stubs"""

    def test_add_plant(self):
        """Test case for add_plant

        
        """
        plant_write = PlantWrite()
        response = self.client.open(
            '/plants',
            method='POST',
            data=json.dumps(plant_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_plant(self):
        """Test case for delete_plant

        
        """
        response = self.client.open(
            '/plants/{plantId}'.format(plant_id='plant_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_plant(self):
        """Test case for get_plant

        
        """
        response = self.client.open(
            '/plants/{plantId}'.format(plant_id='plant_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_plants(self):
        """Test case for get_plants

        
        """
        response = self.client.open(
            '/plants',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_plant(self):
        """Test case for update_plant

        
        """
        plant_write = PlantWrite()
        response = self.client.open(
            '/plants/{plantId}'.format(plant_id='plant_id_example'),
            method='PATCH',
            data=json.dumps(plant_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
