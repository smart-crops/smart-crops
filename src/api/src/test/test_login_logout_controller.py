# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from src.models.authorized import Authorized  # noqa: E501
from src.models.credentials import Credentials  # noqa: E501
from src.test import BaseTestCase


class TestLoginLogoutController(BaseTestCase):
    """LoginLogoutController integration test stubs"""

    def test_login(self):
        """Test case for login

        
        """
        credentials = Credentials()
        response = self.client.open(
            '/login',
            method='POST',
            data=json.dumps(credentials),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_logout(self):
        """Test case for logout

        
        """
        response = self.client.open(
            '/logout',
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
