# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from src.models.authorized import Authorized  # noqa: E501
from src.models.unit_read import UnitRead  # noqa: E501
from src.models.unit_write import UnitWrite  # noqa: E501
from src.test import BaseTestCase


class TestUnitsController(BaseTestCase):
    """UnitsController integration test stubs"""

    def test_add_unit(self):
        """Test case for add_unit

        
        """
        unit_write = UnitWrite()
        response = self.client.open(
            '/units',
            method='POST',
            data=json.dumps(unit_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_unit(self):
        """Test case for delete_unit

        
        """
        response = self.client.open(
            '/units/{unitId}'.format(unit_id='unit_id_example'),
            method='DELETE')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_unit(self):
        """Test case for get_unit

        
        """
        response = self.client.open(
            '/units/{unitId}'.format(unit_id='unit_id_example'),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_units(self):
        """Test case for get_units

        
        """
        response = self.client.open(
            '/units',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_unit(self):
        """Test case for update_unit

        
        """
        unit_write = UnitWrite()
        response = self.client.open(
            '/units/{unitId}'.format(unit_id='unit_id_example'),
            method='PATCH',
            data=json.dumps(unit_write),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
