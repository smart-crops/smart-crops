openapi: 3.0.2
info:
  title: Smart Crops API
  description: >-
    The Smart Crops API (SCAPI) allows the public users to discover and push
    open data related to cyber-agriculture.
  version: 1.0.0
  contact:
    name: Smart Crops API
    url: 'https://gitlab.com/smart-crops'
servers:
  - url: '{scheme}://localhost:5000'
    variables:
      scheme:
        description: 'The Smart Crops API is accessible via https and http.'
        enum:
          - 'https'
          - 'http'
        default: 'http'

tags:
  - name: Login/Logout
    description: Endpoints related to login/logout.
  - name: Environments
    description: Endpoints related to environments.
  - name: Sensors
    description: Endpoints related to sensors.
  - name: Plants
    description: Endpoints related to plants.
  - name: Units
    description: Endpoints related to units.
  - name: Measurements
    description: Endpoints related to measurements.

security:
  - Jwt: []

paths:
  /login:
    post:
      tags: ["Login/Logout"]
      description: "Login to the admin side of the application."
      security: []
      operationId: "login"
      requestBody:
        $ref: '#/components/requestBodies/Credentials'
      responses:
        200:
          $ref: '#/components/responses/Authorized'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
  /logout:
    post:
      tags: ["Login/Logout"]
      description: "Logout from the admin side of the application."
      operationId: "logout"
      responses:
        204:
          description: "Successfully logged out."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'

  /environments:
    get:
      tags: ["Environments"]
      description: "Get all the environments."
      security: []
      operationId: "getEnvironments"
      responses:
        200:
          description: "Environments have been successfully retrieved."
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: '#/components/schemas/EnvironmentRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
    post:
      tags: ["Environments"]
      description: "Create a new environment."
      operationId: "addEnvironment"
      requestBody:
        $ref: '#/components/requestBodies/Environment'
      responses:
        200:
          $ref: '#/components/responses/Authorized'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        409:
          $ref: '#/components/responses/IdConflict'
  /environments/{environmentId}:
    get:
      tags: ["Environments"]
      description: "Get the specified environment."
      security: []
      operationId: "getEnvironment"
      parameters:
        - $ref: '#/components/parameters/environmentId'
      responses:
        200:
          description: "Environment has been successfully retrieved."
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/EnvironmentRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    delete:
      tags: ["Environments"]
      description: "Delete the specified environment."
      operationId: "deleteEnvironment"
      parameters:
        - $ref: '#/components/parameters/environmentId'
      responses:
        204:
          description: "Environment has been successfully deleted."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    patch:
      tags: ["Environments"]
      description: "Update the specified environment."
      operationId: "updateEnvironment"
      parameters:
        - $ref: '#/components/parameters/environmentId'
      requestBody:
        $ref: '#/components/requestBodies/Environment'
      responses:
        204:
          description: "Environment has been successfully updated."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
          
  /sensors:
    get:
      tags: ["Sensors"]
      description: "Get all the sensors."
      security: []
      operationId: "getSensors"
      responses:
        200:
          description: "Sensors have been successfully retrieved."
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: '#/components/schemas/SensorRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
    post:
      tags: ["Sensors"]
      description: "Create a new sensor."
      operationId: "addSensor"
      requestBody:
        $ref: '#/components/requestBodies/Sensor'
      responses:
        200:
          $ref: '#/components/responses/Authorized'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
        409:
          $ref: '#/components/responses/IdConflict'
  /sensors/{sensorId}:
    get:
      tags: ["Sensors"]
      description: "Get the specified sensor."
      security: []
      operationId: "getSensor"
      parameters:
        - $ref: '#/components/parameters/sensorId'
      responses:
        200:
          description: "Sensor has been successfully retrieved."
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SensorRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    delete:
      tags: ["Sensors"]
      description: "Delete the specified sensor."
      operationId: "deleteSensor"
      parameters:
        - $ref: '#/components/parameters/sensorId'
      responses:
        204:
          description: "Sensor has been successfully deleted."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    patch:
      tags: ["Sensors"]
      description: "Update the specified sensor."
      operationId: "updateSensor"
      parameters:
        - $ref: '#/components/parameters/sensorId'
      requestBody:
        $ref: '#/components/requestBodies/Sensor'
      responses:
        204:
          description: "Environment has been successfully updated."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'

  /plants:
    get:
      tags: ["Plants"]
      description: "Get all the plants."
      security: []
      operationId: "getPlants"
      responses:
        200:
          description: "Plants have been successfully retrieved."
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: '#/components/schemas/PlantRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
    post:
      tags: ["Plants"]
      description: "Create a new plant."
      operationId: "addPlant"
      requestBody:
        $ref: '#/components/requestBodies/Plant'
      responses:
        200:
          $ref: '#/components/responses/Authorized'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        409:
          $ref: '#/components/responses/IdConflict'
  /plants/{plantId}:
    get:
      tags: ["Plants"]
      description: "Get the specified plant."
      security: []
      operationId: "getPlant"
      parameters:
        - $ref: '#/components/parameters/plantId'
      responses:
        200:
          description: "Plant has been successfully retrieved."
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/PlantRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    delete:
      tags: ["Plants"]
      description: "Delete the specified plant."
      operationId: "deletePlant"
      parameters:
        - $ref: '#/components/parameters/plantId'
      responses:
        204:
          description: "Plant has been successfully deleted."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    patch:
      tags: ["Plants"]
      description: "Update the specified plant."
      operationId: "updatePlant"
      parameters:
        - $ref: '#/components/parameters/plantId'
      requestBody:
        $ref: '#/components/requestBodies/Plant'
      responses:
        204:
          description: "Plant has been successfully updated."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'

  /units:
    get:
      tags: ["Units"]
      description: "Get all the units."
      security: []
      operationId: "getUnits"
      responses:
        200:
          description: "Units have been successfully retrieved."
          content:
            application/json:
              schema:
                type: "array"
                items:
                  $ref: '#/components/schemas/UnitRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
    post:
      tags: ["Units"]
      description: "Create a new unit."
      operationId: "addUnit"
      requestBody:
        $ref: '#/components/requestBodies/Unit'
      responses:
        200:
          $ref: '#/components/responses/Authorized'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        409:
          $ref: '#/components/responses/IdConflict'
  /units/{unitId}:
    get:
      tags: ["Units"]
      description: "Get the specified unit."
      security: []
      operationId: "getUnit"
      parameters:
        - $ref: '#/components/parameters/unitId'
      responses:
        200:
          description: "Unit has been successfully retrieved."
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UnitRead'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    delete:
      tags: ["Units"]
      description: "Delete the specified unit."
      operationId: "deleteUnit"
      parameters:
        - $ref: '#/components/parameters/unitId'
      responses:
        204:
          description: "Unit has been successfully deleted."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
    patch:
      tags: ["Units"]
      description: "Update the specified unit."
      operationId: "updateUnit"
      parameters:
        - $ref: '#/components/parameters/unitId'
      requestBody:
        $ref: '#/components/requestBodies/Unit'
      responses:
        204:
          description: "Unit has been successfully updated."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'

  /measurements:
    post:
      tags: ["Measurements"]
      description: "Add new measurements."
      operationId: "addMeasurements"
      requestBody:
        $ref: '#/components/requestBodies/Measurements'
      responses:
        200:
          $ref: '#/components/responses/Authorized'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
  /measurements/{unitId}:
    get:
      tags: ["Measurements"]
      description: "Get the specified measurements."
      security: []
      operationId: "getMeasurements"
      parameters:
        - $ref: '#/components/parameters/unitId'
        - $ref: '#/components/parameters/from'
        - $ref: '#/components/parameters/to'
        - $ref: '#/components/parameters/start'
        - $ref: '#/components/parameters/limit'
      responses:
        200:
          description: "Measurements have been successfully retrieved."
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/MeasurementsPagination'
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
        409:
          $ref: '#/components/responses/FiltersConflict'
    delete:
      tags: ["Measurements"]
      description: "Delete the specified measurements."
      operationId: "deleteMeasurements"
      parameters:
        - $ref: '#/components/parameters/unitId'
        - $ref: '#/components/parameters/from'
        - $ref: '#/components/parameters/to'
      responses:
        204:
          description: "Measurements have been successfully deleted."
        401:
          $ref: '#/components/responses/Unauthorized'
        403:
          $ref: '#/components/responses/Forbidden'
        404:
          $ref: '#/components/responses/NotFound'
        409:
          $ref: '#/components/responses/FiltersConflict'

components:
  # Reusable schemas (data models)
  schemas:
    EnvironmentId:
      type: "string"
    SensorId:
      type: "string"
    PlantId:
      type: "string"
    UnitId:
      type: "string"
    Name:
      type: "string"
    Description:
      type: "string"
    Username:
      type: "string"
    Password:
      type: "string"
    Jwt:
      type: "string"
    Latitude:
      type: "number"
    Longitude:
      type: "number"
    Altitude:
      type: "number"
    Timestamp:
      type: "string"
      format: "date-time"
    DataType:
      type: "string"
      enum:
        - "boolean"
        - "integer"
        - "float"
    UnitSymbol:
      type: "string"
    Property:
      type: "string"
    Value:
      type: "string"
    Size:
      type: "integer"
    Start:
      type: "integer"
      minimum: 0
    Limit:
      type: "integer"
      minimum: 0
      maximum: 100

    AdditionalProperties:
      type: "object"
      properties:
        property:
          $ref: '#/components/schemas/Property'
        value:
          $ref: '#/components/schemas/Value'

    Measurements:
      type: "object"
      properties:
        unitSymbol:
          $ref: '#/components/schemas/UnitSymbol'
        value:
          $ref: '#/components/schemas/Value'

    Authorized:
      type: "object"
      properties:
        jwt:
          $ref: '#/components/schemas/Jwt'

    MeasurementsPagination:
      type: "object"
      properties:
        size:
            $ref: '#/components/schemas/Size'
        start:
            $ref: '#/components/schemas/Start'
        limit:
            $ref: '#/components/schemas/Limit'
        measurements:
          type: "array"
          items:
            $ref: '#/components/schemas/MeasurementsRead'

    Credentials:
      type: "object"
      properties:
        username:
          $ref: '#/components/schemas/Username'
        password:
          $ref: '#/components/schemas/Password'
      required:
        - "password"
        - "username"

    EnvironmentRead:
      type: "object"
      properties:
        id:
          $ref: '#/components/schemas/EnvironmentId'
        name:
          $ref: '#/components/schemas/Name'
        description:
          $ref: '#/components/schemas/Description'
        additionalProperties:
          type: "array"
          items:
            $ref: '#/components/schemas/AdditionalProperties'

    EnvironmentWrite:
      type: "object"
      properties:
        name:
          $ref: '#/components/schemas/Name'
        description:
          $ref: '#/components/schemas/Description'
        additionalProperties:
          type: "array"
          items:
            $ref: '#/components/schemas/AdditionalProperties'
      required:
        - "name"
        - "description"

    SensorRead:
      type: "object"
      properties:
        id:
          $ref: '#/components/schemas/SensorId'
        longitude:
          $ref: '#/components/schemas/Longitude'
        latitude:
          $ref: '#/components/schemas/Latitude'
        altitude:
          $ref: '#/components/schemas/Altitude'
        environment:
          $ref: '#/components/schemas/EnvironmentRead'
        additionalProperties:
          type: "array"
          items:
            $ref: '#/components/schemas/AdditionalProperties'

    SensorWrite:
      type: "object"
      properties:
        longitude:
          $ref: '#/components/schemas/Longitude'
        latitude:
          $ref: '#/components/schemas/Latitude'
        altitude:
          $ref: '#/components/schemas/Altitude'
        environmentId:
          $ref: '#/components/schemas/EnvironmentId'
        additionalProperties:
          type: "array"
          items:
            $ref: '#/components/schemas/AdditionalProperties'
      required:
        - "longitude"
        - "latitude"
        - "altitude"
        - "environmentId"

    PlantRead:
      type: "object"
      properties:
        id:
          $ref: '#/components/schemas/PlantId'
        latinName:
          $ref: '#/components/schemas/Name'
        environment:
          $ref: '#/components/schemas/EnvironmentRead'
        additionalProperties:
          type: "array"
          items:
            $ref: '#/components/schemas/AdditionalProperties'

    PlantWrite:
      type: "object"
      properties:
        latinName:
          $ref: '#/components/schemas/Name'
        environmentId:
          $ref: '#/components/schemas/EnvironmentId'
        additionalProperties:
          type: "array"
          items:
            $ref: '#/components/schemas/AdditionalProperties'
      required:
        - "latinName"

    UnitRead:
      type: "object"
      properties:
        id:
          $ref: '#/components/schemas/UnitId'
        name:
          $ref: '#/components/schemas/Name'
        description:
          $ref: '#/components/schemas/Description'
        symbol:
          $ref: '#/components/schemas/UnitSymbol'
        dataType:
          $ref: '#/components/schemas/DataType'

    UnitWrite:
      type: "object"
      properties:
        name:
          $ref: '#/components/schemas/Name'
        description:
          $ref: '#/components/schemas/Description'
        symbol:
          $ref: '#/components/schemas/UnitSymbol'
        dataType:
          $ref: '#/components/schemas/DataType'

    MeasurementsRead:
      type: "object"
      properties:
        timestamp:
          $ref: '#/components/schemas/Timestamp'
        value:
          $ref: '#/components/schemas/Value'

    MeasurementsWrite:
      type: "object"
      properties:
        timestamp:
          $ref: '#/components/schemas/Timestamp'
        measurements:
          type: "array"
          items:
            $ref: '#/components/schemas/Measurements'
      required:
        - "timestamp"
        - "measurements"

  # Reusable path, query, header and cookie parameters
  parameters:
    environmentId:
      name: environmentId
      description: "The environment ID."
      in: path
      schema:
        $ref: '#/components/schemas/EnvironmentId'
      required: true

    sensorId:
      name: sensorId
      description: "The sensor ID."
      in: path
      schema:
        $ref: '#/components/schemas/SensorId'
      required: true

    plantId:
      name: plantId
      description: "The plant ID."
      in: path
      schema:
        $ref: '#/components/schemas/PlantId'
      required: true

    unitId:
      name: unitId
      description: "The unit ID."
      in: path
      schema:
        $ref: '#/components/schemas/UnitId'
      required: true
     
    from:
      name: from
      description: "The starting timestamp to get the data."
      in: query
      schema:
        $ref: '#/components/schemas/Timestamp'
      required: true
     
    to:
      name: to
      description: "The ending timestamp to get the data."
      in: query
      schema:
        $ref: '#/components/schemas/Timestamp'
      required: true

    start:
      name: start
      description: "The starting item of all data."
      in: query
      schema:
        $ref: '#/components/schemas/Start'
      required: true

    limit:
      name: limit
      description: "The limit of the number of items to get of all data."
      in: query
      schema:
        $ref: '#/components/schemas/Limit'
      required: true

  # Reusable request bodies
  requestBodies:
    Credentials:
      description: "The credentials of the user."
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Credentials'

    Environment:
      description: "The details of the environment."
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EnvironmentWrite'

    Sensor:
      description: "The details of the sensor."
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/SensorWrite'
          examples:
            SensorWriteExample:
              $ref: '#/components/examples/SensorWriteExample'

    Plant:
      description: "The details of the plant."
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/PlantWrite'

    Unit:
      description: "The details of the unit."
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/UnitWrite'

    Measurements:
      description: "The details of the measurement."
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/MeasurementsWrite'

  # Security scheme definitions (see Authentication)
  securitySchemes:
    Jwt:
      type: http
      scheme: bearer
      bearerFormat: JWT
      x-bearerInfoFunc: src.services.JwtService.decode_token

  # Reusable responses, such as 401 Unauthorized or 400 Bad Request
  responses:
    Authorized:
      description: "The entity has been successfully authenticated."
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Authorized'
    IdConflict:
      description: "A conflict occurs: ID is already used. Please change the inputs and try again."
    FiltersConflict:
      description: "A conflict occurs: filters are ambigious. Please change the inputs and try again."
    Unauthorized:
      description: "Wrong username/password or authentication token."
    Forbidden:
      description: "The entity do not have the rights to access the ressource."
    NotFound:
      description: "The specified resource was not found."

  # Reusable response headers
  #headers:
  
  # Reusable examples
  examples:
    SensorWriteExample:
      summary: Basic sensor.
      value:
        longitude: 1
        latitude: 1
        altitude: 1
        environmentId: 1
  
  # Reusable links
  #links:

  # Reusable callbacks
  #callbacks: