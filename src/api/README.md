# Smart crops - `src/api/`

This part of the application depends on:

- [`src/db/`](https://gitlab.com/smart-crops/smart-crops/tree/master/src/db)

## Requirements

- [`docker`](https://www.docker.com/)
- [`docker-compose`](https://docs.docker.com/compose/)
- [`git`](https://git-scm.com/)
- [`python3`](https://python.org)

## Launch

### Locally

```sh
# Clone the repository
git clone https://gitlab.com/smart-crops/smart-crops.git

# Move to the cloned directory
cd smart-crops/src/api/

# Copy the environment variables file
cp .env.dist .env

# Edit the environment variables file
vim .env

# Create the virtual environment
python3 -m venv .venv

# Source the virtual environment
source .venv/bin/activate

# Install the dependencies
pip3 install --user -r requirements.txt

# Start the API
python3 -m src
```

### Docker

```sh
# Clone the repository
git clone https://gitlab.com/smart-crops/smart-crops.git

# Move to the cloned directory
cd smart-crops

# Copy the environment variables file
cp .env.dist .env

# Edit the environment variables file
vim .env

# Start the API
docker-compose up --build api
```

## Development

```sh
# Get OpenAPI Generator
wget http://central.maven.org/maven2/org/openapitools/openapi-generator-cli/3.3.4/openapi-generator-cli-3.3.4.jar -O openapi-generator-cli.jar

# Generate skeleton
java -jar openapi-generator-cli.jar generate -i src/openapi/openapi.yaml -g python-flask -c src/openapi/config.json
```
