# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "src"
VERSION = "1.0.0"

# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = [
    "connexion==2.0.0",
    "swagger-ui-bundle==0.0.2",
    "python_dateutil==2.6.0"
]

setup(
    name=NAME,
    version=VERSION,
    description="Smart Crops API",
    author_email="",
    url="",
    keywords=["OpenAPI", "Smart Crops API"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['openapi/openapi.yaml']},
    include_package_data=True,
    entry_points={
        'console_scripts': ['src=src.__main__:main']},
    long_description="""\
    The Smart Crops API (SCAPI) allows the public users to discover and push open data related to cyber-agriculture.
    """
)

